import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CursosFormComponent } from './cursos-form/cursos-form.component';
import { CursosListaComponent } from './cursos-lista/cursos-lista.component';
import { CursosRoutingModule } from './cursos-routing.module';


@NgModule({
  imports: [
    CommonModule,
    CursosRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [CursosListaComponent, CursosFormComponent],
})
export class CursosModule { }
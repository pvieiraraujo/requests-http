import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AlertModalService } from './../../shared/alert-modal.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { EMPTY, empty, Observable, Subject } from 'rxjs';
import { catchError, switchMap, take } from 'rxjs/operators';
import { Curso } from './../curso';
import { CursosService } from './../cursos.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-cursos-lista',
  templateUrl: './cursos-lista.component.html',
  styleUrls: ['./cursos-lista.component.scss']
})
export class CursosListaComponent implements OnInit {

  // bsModalRef: BsModalRef;

  deleteModalRef: BsModalRef;
  @ViewChild('deleteModal') deleteModal;
 
  cursos$: Observable<Curso[]>;
  error$ = new Subject<boolean>();

  cursoSelecionado: Curso;

  constructor(private service: CursosService,
    private modalService: BsModalService,
    private alertService: AlertModalService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    // this.service.list()
    //   .subscribe(dados => this.cursos = dados);

    this.onRefresh();
  }

  onRefresh() {
    this.cursos$ = this.service.list()
      .pipe(
        catchError(error => {
          console.error(error);
          // this.error$.next(true);
          this.handleError();
          return empty();
        })
      );

    // this.service.list().subscribe(
    //   dados => {
    //     console.log(dados);
    //   },
    //   error => console.error(error),
    //   () => console.log('Observable completo!')
    // )
  }

  handleError(){
    this.alertService.showAlertDanger('Error ao carregar cursos. Tente novamente mais tarde');
  }

  onEdit(id){
    this.router.navigate(['editar', id], { relativeTo: this.route});
  }

  onDelete(object){
    this.cursoSelecionado = object;
    // this.deleteModalRef = this.modalService.show(this.deleteModal, {class: 'modal-sm'});
    const result$ = this.alertService.showConfirm('Confirmação', 'Tem certeza que deseja remover o curso?', 'Sim', 'Cancelar');
    result$.asObservable()
    .pipe(
      take(1),
      switchMap(result => result ? this.service.remove(object.id): EMPTY)
    )
    .subscribe( 
      success => {
      this.onRefresh()
    },
    error => this.alertService.showAlertDanger('Error ao remover o curso. Tente novamente mais tarde'));
  }

  onConfirmDelete(){
    this.service.remove(this.cursoSelecionado.id)
    .subscribe();
    success => {
      this.deleteModalRef.hide(),
      this.onRefresh()
    };
    error => this.alertService.showAlertDanger('Error ao remover o curso. Tente novamente mais tarde');
  }

  onDeclineDelete(){
    this.deleteModalRef.hide();
  }

}

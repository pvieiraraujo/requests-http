import { ReactiveSearchModule } from './reactive-search/reactive-search.module';
import { ReactiveSearchRoutingModule } from './reactive-search/reactive-search-routing.module';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CursosModule } from './cursos/cursos.module';
import { UnsubscribeRxjsModule } from './unsubscribe-rxjs/unsubscribe-rxjs.module';
import { UploadFileModule } from './upload-file/upload-file.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'busca-reativa',
  },
  {
    path: 'cursos',
    loadChildren: () => import('./cursos/cursos.module').then((m) => CursosModule),
  },
  {
    path: 'upload',
    loadChildren: () => import('./upload-file/upload-file.module').then((m) => UploadFileModule),
  },
  {
    path: 'rxjs-poc',
    loadChildren: () => import('./unsubscribe-rxjs/unsubscribe-rxjs.module').then((m) => UnsubscribeRxjsModule
      ),
  },
  {
    path: 'busca-reativa',
    loadChildren: () => import('./reactive-search/reactive-search.module').then((m) => ReactiveSearchModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }

import { PocTakeComponent } from './componentes/poc.take.component';
import { PocTakeUntilComponent } from './componentes/poc.take.until.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PocAsyncComponent } from './componentes/poc-async.component';
import { PocUnsubComponent } from './componentes/poc-unsub.component';
import { PocComponent } from './componentes/poc.component';
import { PocBaseComponent } from './poc-base/poc-base.component';
import { UnsubscribePocComponent } from './unsubscribe-poc/unsubscribe-poc.component';
import { UnsubscribeRxjsRoutingModule } from './unsubscribe-rxjs-routing.module';



@NgModule({
  imports: [
    CommonModule,
    UnsubscribeRxjsRoutingModule
  ],
  declarations: [
    UnsubscribePocComponent,
    PocComponent,
    PocAsyncComponent,
    PocTakeUntilComponent,
    PocBaseComponent,
    PocUnsubComponent,
    PocBaseComponent,
    PocTakeComponent
  ]
})
export class UnsubscribeRxjsModule {}